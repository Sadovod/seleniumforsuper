import pytest
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from configurations import PATH_TO_DRIVER


@pytest.fixture()
def driver():
    # запуск chrome драйвера
    chrome_driver = WebDriver(executable_path=f'{PATH_TO_DRIVER}')
    chrome_driver.implicitly_wait(5)
    # переход на страницу авторизации
    chrome_driver.get('http://super-admin.tabolabs.com/')
    yield chrome_driver
    chrome_driver.quit()


def test_login_page(driver: WebDriver):
    # сочетания (логин, пароль, тип данных)
    login_and_password = [
        ('Sadovod@super.ru', '12345', 'корректные данные'),
        ('1 /.+"', '2-+ "/', 'некорректные данные'),
        ('', '', 'пустые данные'),
        ('', '123', 'только пароль'),
        ('peppa', '', 'только логин')
    ]

    for data in login_and_password:
        # ввод логина
        driver.find_element_by_id('normal_login_email').send_keys(data[0])
        # ввод пароля
        driver.find_element_by_id('normal_login_password').send_keys(data[1])
        # нажатие на кнопку "Войти"
        driver.find_element_by_xpath('//button[@type="submit"]').click()

        # Если не введен логин
        if data[0] == '' and data[1] != '':
            WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, '//div[@class="ant-form-explain"]')))
            # проверка наличия требования "Введите логин!"
            assert driver.find_element_by_xpath('//div[@class="ant-form-explain"]').text == 'Введите логин!'

        # Если не введен пароль
        if data[0] != '' and data[1] == '':
            WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, '//div[@class="ant-form-explain"]')))
            # проверка наличия требования "Введите пароль!"
            assert driver.find_element_by_xpath('//div[@class="ant-form-explain"]').text == 'Введите пароль!'

        # Если логин и пароль не введены
        if data[0] == '' and data[1] == '':
            WebDriverWait(driver, 3).until(EC.visibility_of_all_elements_located((By.XPATH, '//div[@class="ant-form-explain"]')))
            # проверка наличия требования "Введите логин!"
            assert driver.find_elements_by_xpath('//div[@class="ant-form-explain"]')[0].text == 'Введите логин!'
            WebDriverWait(driver, 3).until(EC.visibility_of_all_elements_located((By.XPATH, '//div[@class="ant-form-explain"]')))
            # проверка наличия требования "Введите пароль!"
            assert driver.find_elements_by_xpath('//div[@class="ant-form-explain"]')[1].text == 'Введите пароль!'

        # Проверка что URL не изменился, так как введены неверные данные
        assert driver.current_url == 'http://super-admin.tabolabs.com/'

        # перезагрузка страницы, для очищения полей логина и пароля
        driver.refresh()
