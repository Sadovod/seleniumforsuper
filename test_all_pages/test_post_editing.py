import time
import pytest
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from configurations import PATH_TO_PICTURE, PATH_TO_VIDEO, TEST_WIDGET_CODE, PATH_TO_WIDGET_SCREENSHOT, PATH_TO_PICTURE_2


@pytest.fixture(scope="module")
def driver(general_fixture):
    general_fixture.get('http://super-admin.tabolabs.com/posts/new')
    yield general_fixture


def test_editing_blocks(driver):
    """ СОЗДАНИЕ ПОСТА """

    """ Обязательные блоки """
    # заголовок поста
    title = 'Тестовый заголовок'
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, '//div[text()="Заголовок"]/..//textarea')))
    # ввод заголовка
    driver.find_element_by_xpath('//div[text()="Заголовок"]/..//textarea').send_keys(title)

    # ввод короткого заголовка
    driver.find_element_by_xpath('//div[text()="Короткий заголовок"]/..//textarea').send_keys('Тестовый короткий заголовок')

    # ввод подзаголовка
    driver.find_element_by_xpath('//div[text()="Подзаголовок"]/..//textarea').send_keys('Тестовый подзаголовок')

    # ввод подписи для обложки
    driver.find_element_by_xpath('//div[text()="Обложка"]/..//textarea').send_keys('Тестовая обложка')
    # выбор последнего цвета подписи (розовый цвет)
    driver.find_element_by_xpath('//div[text()="Обложка"]/..//div[text()="Выбрать цвет подписи"]').click()
    WebDriverWait(driver, 3).until(EC.visibility_of_all_elements_located((By.XPATH, '//div[@class="twitter-picker "]/div/span/div')))
    driver.find_elements_by_xpath('//div[@class="twitter-picker "]/div/span/div')[-1].click()
    # проверка того, что цвет действительно розовый
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.CSS_SELECTOR, 'div[class*="BlockWrapper"]  textarea[style*="rgb(250, 42, 93)"]')))
    # загрузка фотографии с компьютера (ввод полного пути к фото)
    driver.find_element_by_xpath('//div[text()="Обложка"]/..//input[@type="file"]').send_keys(PATH_TO_PICTURE)
    # ожидание загрузки фото
    WebDriverWait(driver, 6).until(EC.visibility_of_element_located((By.CSS_SELECTOR, 'div[class*="BlockWrapper"] img[src*="tabolabs"]')))
    # нажатие на удаление загруженного фото
    driver.find_element_by_xpath('//div[text()="Обложка"]/..//div[text()="X Удалить"]').click()
    # ожидание появления дефолтного изображения
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.CSS_SELECTOR, 'div[class*="BlockWrapper"] img[src*="preview-default.jpg"]')))
    # загрузка второй фотографии (т.к. первая фотография повторно не загружается)
    driver.find_element_by_xpath('//div[text()="Обложка"]/..//input[@type="file"]').send_keys(PATH_TO_PICTURE_2)

    """ Проверка удаления дополнительного блока """
    # открытие панели дополнительных блоков редактирования
    driver.find_elements_by_xpath('//div[@class="Editor__add-block"]/span')[-1].click()
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, '//div[@class="Editor__editor-root"]//*[@title="Заголовок второго уровня"]')))
    # выбор блока "Заголовок второго уровня"
    driver.find_element_by_xpath('//div[@class="Editor__editor-root"]//*[@title="Заголовок второго уровня"]').click()
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, '//div[@class="Editor__buttons"][1]/span[@data-index][1]')))
    # нажатие на удаление блока
    driver.find_element_by_xpath('//div[@class="Editor__buttons"][1]/span[@data-index][1]').click()
    WebDriverWait(driver, 3).until(EC.alert_is_present())
    # переключение на появившееся "окно подтверждения" и подтверждение удаления
    driver.switch_to.alert.accept()

    """ Блок заголовка второго уровня """
    WebDriverWait(driver, 3).until(EC.visibility_of_all_elements_located((By.XPATH, '//div[@class="Editor__add-block"]/span')))
    # открытие панели дополнительных блоков редактирования
    driver.find_elements_by_xpath('//div[@class="Editor__add-block"]/span')[-1].click()
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, '//div[@class="Editor__editor-root"]//*[@title="Заголовок второго уровня"]')))
    # выбор блока "Заголовок второго уровня"
    driver.find_element_by_xpath('//div[@class="Editor__editor-root"]//*[@title="Заголовок второго уровня"]').click()
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, '//div[text()="Заголовок второго уровня"]/..//textarea')))
    # ввод заголовка второго уровня
    driver.find_element_by_xpath('//div[text()="Заголовок второго уровня"]/..//textarea').send_keys('Тестовый заголовок второго уровня')

    """ Блок текста """
    WebDriverWait(driver, 3).until(EC.visibility_of_all_elements_located((By.XPATH, '//div[@class="Editor__add-block"]/span')))
    # открытие панели дополнительных блоков редактирования
    driver.find_elements_by_xpath('//div[@class="Editor__add-block"]/span')[-1].click()
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, '//div[@class="Editor__editor-root"]//*[@title="Блок с текстом"]')))
    # выбор блока текста
    driver.find_element_by_xpath('//div[@class="Editor__editor-root"]//*[@title="Блок с текстом"]').click()
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, '//div[text()="Текст"]/..//div[@role="textbox"]')))
    # ввод текста
    driver.find_element_by_xpath('//div[text()="Текст"]/..//div[@role="textbox"]').send_keys('Тестовый текст')
    # выделение набранного текста для перехода к панели изменения текста
    driver.find_element_by_xpath('//div[text()="Текст"]/..//div[@role="textbox"]').send_keys(Keys.CONTROL + 'a')
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.CSS_SELECTOR, 'li[class*=t-bold]')))
    # выбор жирного шрифта
    driver.find_element_by_css_selector('li[class*=t-bold]').click()
    # проверка изменения
    WebDriverWait(driver, 2).until(EC.visibility_of_element_located((By.CSS_SELECTOR, 'span[style*=bold]')))
    # выбор курсивного шрифта
    driver.find_element_by_css_selector('li[class*=t-italic]').click()
    # проверка изменения
    WebDriverWait(driver, 2).until(EC.visibility_of_element_located((By.CSS_SELECTOR, 'span[style*=italic]')))
    # выбор подчеркнутого шрифта
    driver.find_element_by_xpath('//div[text()="Текст"]/..//li[3]').click()
    WebDriverWait(driver, 2).until(EC.visibility_of_element_located((By.CSS_SELECTOR, 'span[style*=underline]')))
    # выбор непронумерованного списка
    driver.find_element_by_xpath('//div[text()="Текст"]/..//li[5]').click()
    # проверка изменения
    WebDriverWait(driver, 2).until(EC.visibility_of_element_located((By.XPATH, '//div[text()="Текст"]/..//ul//span[text()="Тестовый текст"]')))
    # выбор пронумерованного списка
    driver.find_element_by_xpath('//div[text()="Текст"]/..//li[6]').click()
    # проверка изменения
    WebDriverWait(driver, 2).until(EC.visibility_of_element_located((By.XPATH, '//div[text()="Текст"]/..//ol//span[text()="Тестовый текст"]')))
    # выбор перевода текста в нижний индекс
    driver.find_element_by_xpath('//div[text()="Текст"]/..//li[7]').click()
    # проверка изменения
    WebDriverWait(driver, 2).until(EC.visibility_of_element_located((By.CSS_SELECTOR, 'span[style*=vertical-align][style*=sub]')))
    # выбор перевода текста в верхний индекс
    driver.find_element_by_xpath('//div[text()="Текст"]/..//li[8]').click()
    # проверка изменения
    WebDriverWait(driver, 2).until(EC.visibility_of_element_located((By.CSS_SELECTOR, 'span[style*=vertical-align][style*=super]')))
    # выбор замены текста на гиперссылку
    driver.find_element_by_xpath('//div[text()="Текст"]/..//li[4]').click()
    WebDriverWait(driver, 3).until(EC.alert_is_present())
    # переключение на появившееся "окно подтверждения" и ввод тестового адреса для гиперссылки
    driver.switch_to.alert.send_keys('http://test.url')
    # подтверждение изменения
    driver.switch_to.alert.accept()
    # проверка изменения
    WebDriverWait(driver, 2).until(EC.visibility_of_element_located((By.CSS_SELECTOR, 'a[href="http://test.url"][class*=TextEditor]')))

    """ Блок фотографии """
    WebDriverWait(driver, 3).until(EC.visibility_of_all_elements_located((By.XPATH, '//div[@class="Editor__add-block"]/span')))
    # открытие панели дополнительных блоков редактирования
    driver.find_elements_by_xpath('//div[@class="Editor__add-block"]/span')[-1].click()
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, '//div[@class="Editor__editor-root"]//*[@title="Фотография"]')))
    # выбор блока фотографии
    driver.find_element_by_xpath('//div[@class="Editor__editor-root"]//*[@title="Фотография"]').click()
    WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.XPATH, '//div[text()="Фотография"]/..//input[@type="file"]')))
    # загрузка фотографии с компьютера (ввод полного пути к фото)
    driver.find_element_by_xpath('//div[text()="Фотография"]/..//input[@type="file"]').send_keys(PATH_TO_PICTURE)
    # ожидание загрузки фото
    WebDriverWait(driver, 6).until(EC.presence_of_element_located((By.XPATH, '//div[text()="Фотография"]/..//div[text()="X Удалить"]')))
    # нажатие на удаление загруженного фото
    driver.find_element_by_xpath('//div[text()="Фотография"]/..//div[text()="X Удалить"]').click()
    # ожидание появления дефолтного изображения
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.CSS_SELECTOR, 'div[class*="Editor__block"] img[src*="preview-default.jpg"]')))
    # ввод подписи для фотографии
    driver.find_element_by_xpath('//div[text()="Фотография"]/..//textarea').send_keys('Тестовая фотография')
    # выбор цвета подписи
    driver.find_element_by_xpath('//div[text()="Фотография"]/..//div[text()="Выбрать цвет подписи"]').click()
    WebDriverWait(driver, 3).until(EC.visibility_of_all_elements_located((By.XPATH, '//div[@class="twitter-picker "]/div/span/div')))
    # выбор последнего цвета (розовый цвет)
    driver.find_elements_by_xpath('//div[@class="twitter-picker "]/div/span/div')[-1].click()
    # проверка того, что цвет действительно розовый
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.CSS_SELECTOR, 'div[class*="Editor__block"]  textarea[style*="rgb(250, 42, 93)"]')))

    """ Блок виджета """
    # открытие панели дополнительных блоков редактирования
    driver.find_elements_by_xpath('//div[@class="Editor__add-block"]/span')[-1].click()
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, '//div[@class="Editor__editor-root"]//*[@title="Виджет"]')))
    # выбор блока виджета
    driver.find_element_by_xpath('//div[@class="Editor__editor-root"]//*[@title="Виджет"]').click()
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, '//div[text()="Код виджета"]/..//textarea')))
    # ввод кода для виджета
    driver.find_element_by_xpath('//div[text()="Код виджета"]/..//textarea').send_keys(TEST_WIDGET_CODE)
    # сохранение скриншота виджета по указанному пути
    driver.find_elements_by_css_selector('div[class*=widget-editor-preview]')[1].screenshot(PATH_TO_WIDGET_SCREENSHOT)

    """ Блок видео """
    # открытие панели дополнительных блоков редактирования
    driver.find_elements_by_xpath('//div[@class="Editor__add-block"]/span')[-1].click()
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, '//div[@class="Editor__editor-root"]//*[@title="Видео"]')))
    # выбор блока видео
    driver.find_element_by_xpath('//div[@class="Editor__editor-root"]//*[@title="Видео"]').click()
    WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.XPATH, '//div[text()="Видео"]/..//input[@type="file"]')))
    # загрузка видео с компьютера (ввод полного пути к видео)
    driver.find_element_by_xpath('//div[text()="Видео"]/..//input[@type="file"]').send_keys(PATH_TO_VIDEO)
    # ожидание появления видео
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.CSS_SELECTOR, 'div[class*="VideoEditor"] video[src*="tabolabs"]')))
    # нажатие на удаление загруженного видео
    driver.find_element_by_xpath('//div[text()="Видео"]/..//div[text()="X Удалить"]').click()
    # ожидание появления дефолтного изображения
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.CSS_SELECTOR, 'div[class*="VideoEditor"] img[src*="preview-default.jpg"]')))
    # ввод подписи для видео
    driver.find_element_by_xpath('//div[text()="Видео"]/..//textarea').send_keys('Тестовое видео')

    """ Перетаскивание блоков """
    block_drag = ActionChains(driver)
    # объявление перетаскиваемого элемента (объект)
    # (в данном случае это последний блок "блок видео")
    source = driver.find_elements_by_css_selector('div[class*=Editor__block] span[draggable]')[-1]
    # объявление места, куда следует перетащить объект (цель)
    # (в данном случае это первый дополнительный блок "блок заголовка второго уровня")
    target = driver.find_elements_by_css_selector('div[class*=Editor__block] span[draggable]')[0]
    # 1) перемещение курсора к объекту; 2) команда "нажать и держать"; 3) небольшое перемещение курсора;
    # 4) перемещение курсора к цели; 5) небольшое перемещение курсора; 6) команда "отпустить кнопку мыши"
    # без небольшого перемещения курсора объект не перетаскивается
    block_drag.move_to_element(source).click_and_hold().move_by_offset(0, 10).move_to_element(target).move_by_offset(0, -2).release().perform()
    time.sleep(1)
    # определение названия первого дополнительного блока редактирования
    first_added_block_name = driver.find_elements_by_xpath('//div[@class="Editor__title"]')[0].text
    # проверка того, что первым блоком стал "блок видео"
    assert first_added_block_name == 'Видео'

    """ Применение обязательных фильтров """
    # нажатие на фильтр "Тип поста"
    driver.find_element_by_xpath('//div[text()="Тип поста"]/..//div[@aria-expanded]').click()
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, '//li[@role="option" and text()="Новость"]')))
    # выбор типа "Новость"
    driver.find_element_by_xpath('//li[@role="option" and text()="Новость"]').click()
    # нажатие на фильтр "Рубрика"
    driver.find_element_by_xpath('//div[text()="Рубрика"]/..//div[@aria-expanded]').click()
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, '//li[@role="option" and text()="Галерея"]')))
    # выбор рубрики "Галерея"
    driver.find_element_by_xpath('//li[@role="option" and text()="Галерея"]').click()

    """ Сохранение поста """
    # нажатие на кнопку сохранения поста
    driver.find_element_by_xpath('//button/span[text()="Сохранить"]/..').click()
    # ожидание уведомления об успешном сохранении
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, '//div[@class="ant-message"]//span[text()="Успешно!"]')))
    # переход на страницу постов
    driver.find_element_by_css_selector('a[href="/posts"][class*="_348G6"]').click()
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, '//div[@class="_1b-uQ"]/div[1]//div[@class="gSBJN"]/a')))
    # определение имени первого поста
    first_post_name = driver.find_element_by_xpath('//div[@class="_1b-uQ"]/div[1]//div[@class="gSBJN"]/a').text
    # подтверждение того что первым постом является наш ранее сохраненный тестовый пост
    assert first_post_name == title

    """ Удаление поста """
    # переход на редактирование первого поста
    driver.find_element_by_xpath('//div[@class="_1b-uQ"]/div[1]//div[@class="gSBJN"]/a').click()
    WebDriverWait(driver, 4).until(EC.visibility_of_element_located((By.XPATH, '//span[text()="Удалить"]/..')))
    # нажатие на кнопку удаления поста
    driver.find_element_by_xpath('//span[text()="Удалить"]/..').click()
    WebDriverWait(driver, 4).until(EC.visibility_of_element_located((By.XPATH, '//div[@class="ant-popover-buttons"]//span[text()="Да"]/..')))
    # подтверждение удаления в появившейся рамке
    driver.find_element_by_xpath('//div[@class="ant-popover-buttons"]//span[text()="Да"]/..').click()
    # ожидание автоперехода на страницу постов
    WebDriverWait(driver, 4).until(EC.visibility_of_element_located((By.XPATH, '//h1[text()="Посты"]')))
    # определение имени первого поста
    first_post_name = driver.find_element_by_xpath('//div[@class="_1b-uQ"]/div[1]//div[@class="gSBJN"]/a').text
    # подтверждение того, что первый пост не является ранее созданым нами тестовым постом
    assert first_post_name != title
