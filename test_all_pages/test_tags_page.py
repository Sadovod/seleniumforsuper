import pytest
import time
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException


# корректное имя тега
valid_name = 'test_name'
# некорректное имя тега
invalid_name = '!1@#$%^ &*({/|.,'


@pytest.fixture()
def driver(general_fixture: WebDriver):
    # переход на страницу тегов
    general_fixture.get('http://super-admin.tabolabs.com/tags')
    WebDriverWait(general_fixture, 3).until(EC.visibility_of_element_located((By.XPATH, "//h1[contains(text(), 'Теги')]")))
    yield general_fixture


def test_create_tag(driver: WebDriver):
    """ СОЗДАНИЕ ТЕГА """

    # нажатие на кнопку создания тега
    driver.find_element(By.CLASS_NAME, "_3ugjY").click()
    # ввод корректного имени тега
    # если такое имя занято, то добавляем к имени "7" (не больше 8-ми семерок)
    for i in range(8):
        # корректное имя
        global valid_name
        # очищение поля имени тега
        driver.find_element(By.CLASS_NAME, "ant-input").send_keys(Keys.CONTROL + 'a' + Keys.BACKSPACE)
        # ввод корректного имени тега
        driver.find_element(By.CLASS_NAME, "ant-input").send_keys(valid_name)
        WebDriverWait(driver, 3).until(EC.element_to_be_clickable((By.XPATH, '//div[@class="ant-modal-footer"]/button[2]')))
        # нажатие на кнопку "Создать"
        driver.find_element_by_xpath('//div[@class="ant-modal-footer"]/button[2]').click()
        # ожидание закрытие окна создания тега,
        # если окно не закрылось, значит введенное имя тега занято
        try:
            WebDriverWait(driver, 2).until_not(EC.visibility_of_element_located((By.XPATH, '//input[@placeholder="Введите название тега"]')))
            break
        except TimeoutException:
            # добавление цифры "7" к имени тега
            valid_name += '7'

    # проверка наличия созданного тега
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, f"//div[@class='ok_jQ']//span[contains(text(), '{valid_name}')]")))


def test_edit_tag(driver: WebDriver):
    """ РЕДАКТИРОВАНИЕ ТЕГА """

    """ Ввод пустого имени """
    time.sleep(1)
    # переход на редактирование первого тега
    driver.find_element_by_xpath('//div[@class="ok_jQ"]/div[1]//span[@class="z_97e"]').click()
    # выделение имени тега и его последующее удаление
    driver.find_element(By.CLASS_NAME, "ant-input").send_keys(Keys.CONTROL + 'a' + Keys.BACKSPACE)
    # сохранение изменений (кнопка "Сохранить")
    driver.find_element_by_xpath('//div[@class="ant-modal-footer"]/button[2]').click()
    # проверка того, что изменения НЕ сохранились
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, f'//div[@class="ok_jQ"]//span[contains(text(), "{valid_name}")]')))

    """ Ввод некорректного имени """
    time.sleep(1)
    # переход на редактирование первого тега
    driver.find_element_by_xpath('//div[@class="ok_jQ"]/div[1]//span[@class="z_97e"]').click()
    # выделение имени тега и его последующее удаление
    driver.find_element(By.CLASS_NAME, "ant-input").send_keys(Keys.CONTROL + 'a' + Keys.BACKSPACE)
    # ввод некорректного имени
    driver.find_element(By.CLASS_NAME, "ant-input").send_keys(invalid_name)
    # сохранение изменений (кнопка "Сохранить")
    driver.find_element_by_xpath('//div[@class="ant-modal-footer"]/button[2]').click()
    # проверка того, что изменения сохранились
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, f'//div[@class="ok_jQ"]//span[contains(text(), "{invalid_name}")]')))

    """ Ввод занятого имени """
    time.sleep(1)
    # сохраняем в переменную имя второго по списку тега
    duplicate_name = driver.find_element_by_xpath('//div[@class="ok_jQ"]/div[2]//span[@class="z_97e"]').text
    # переход на редактирование первого тега
    driver.find_element_by_xpath('//div[@class="ok_jQ"]/div[1]//span[@class="z_97e"]').click()
    # выделение имени тега и его последующее удаление
    driver.find_element(By.CLASS_NAME, "ant-input").send_keys(Keys.CONTROL + 'a' + Keys.BACKSPACE)
    # ввод занятого имени (имя второго по списку тега)
    driver.find_element(By.CLASS_NAME, "ant-input").send_keys(duplicate_name)
    # сохранение изменений (кнопка "Сохранить")
    driver.find_element_by_xpath('//div[@class="ant-modal-footer"]/button[2]').click()
    time.sleep(1)
    # сохранение не должно было произойти, вследствии того, что имя занято
    # нажатие на кнопку "Закрыть"
    driver.find_element_by_xpath('//span[text()="Закрыть"]/..').click()
    # проверка того, что изменения не сохранились
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, f'//div[@class="ok_jQ"]//span[contains(text(), "{invalid_name}")]')))


def test_delete_tag(driver: WebDriver):
    """ УДАЛЕНИЕ ТЕГА """

    WebDriverWait(driver, 3).until(EC.element_to_be_clickable((By.XPATH, '//div[@class="ok_jQ"]/div[1]//span[contains(text(), "Удалить")]')))
    # нажатие на кнопку "Удалить" для удаления первого тега
    driver.find_element_by_xpath('//div[@class="ok_jQ"]/div[1]//span[contains(text(), "Удалить")]').click()
    WebDriverWait(driver, 3).until(EC.element_to_be_clickable((By.XPATH, '//div[@class="ant-popover-buttons"]/button[2]')))
    # подтверждение удаления в появившейся рамке
    driver.find_element_by_xpath('//div[@class="ant-popover-buttons"]/button[2]').click()
    # проверка того, что тег удален
    WebDriverWait(driver, 7).until_not(EC.visibility_of_element_located((By.XPATH, f'//div[@class="ok_jQ"]//span[contains(text(), "{invalid_name}")]')))


def test_finding_tags(driver: WebDriver):
    """ ПОИСК ТЕГОВ """

    # скролл меню разделов сайта (чтобы следующий скролл относился к странице тегов)
    driver.find_element_by_tag_name('body').send_keys(Keys.END)
    for i in range(5):
        # определение всех отображенных тегов
        all_tags = driver.find_elements_by_xpath('//span[@class="z_97e"]')
        # скролл вниз
        driver.find_element_by_tag_name('body').send_keys(Keys.END)
        # проверка того, что загрузились новые теги,
        # в противном случае прерывание цикла
        try:
            WebDriverWait(driver, 2).until(EC.visibility_of_element_located((By.XPATH, '//div[@class="ok_jQ"]/div[{}]//span[@class="z_97e"]'.format(len(all_tags)+1))))
        except TimeoutException:
            break

    # сохранение в список названий всех отображенных постов
    all_tags_names = [tg.text for tg in driver.find_elements_by_xpath('//span[@class="z_97e"]')]
    # сохранение в множество всех символов (в единичном экземпляре), использованных в названиях отображенных тегов
    all_symbols = set(list(" ".join(all_tags_names)))

    """ Поиск без совпадений """
    # ввод текста в поле поиска тега с последующим нажатием ENTER
    # если имеются совпадения, то операция повторяется
    for i in range(5):
        # ввод текста и нажатие ENTER
        driver.find_element_by_xpath('//input[@placeholder="Поиск"]').send_keys('TestTag' + Keys.ENTER)
        # проверка наличия совпадений
        try:
            WebDriverWait(driver, 2).until_not(EC.visibility_of_all_elements_located((By.CLASS_NAME, '_3PGYQ')))
            break
        except TimeoutException:
            continue
    else:
        raise TimeoutException
    # очищение поля поиска тегов
    driver.find_element_by_xpath('//input[@placeholder="Поиск"]').send_keys(Keys.CONTROL + 'a' + Keys.BACKSPACE)

    """ Поиск с одним совпадением """
    # сохранение названия первого тега в переменную
    search_text = driver.find_element_by_xpath('//div[@class="ok_jQ"]/div[1]//span[@class="z_97e"]').text
    # ввод названия первого тега и нажатие на ENTER
    driver.find_element_by_xpath('//input[@placeholder="Поиск"]').send_keys(search_text + Keys.ENTER)
    # проверка того, что найдено только одно совпадение
    assert len(driver.find_elements_by_xpath('//div[@class="_3TF_T"]')) == 1
    # очищение поля поиска тегов
    driver.find_element_by_xpath('//input[@placeholder="Поиск"]').send_keys(Keys.CONTROL + 'a' + Keys.BACKSPACE)

    """ Поиск с несколькими совпадениями """
    # поиск символа, который встречается сразу в нескольких названиях тегов и ввод этого символа
    # с последующим нажатием на ENTER
    for symbol in all_symbols:
        i = 0
        for name in all_tags_names:
            if symbol in name:
                i += 1
            if i >= 2:
                # ввод символа и нажатие на ENTER
                driver.find_element_by_xpath('//input[@placeholder="Поиск"]').send_keys(symbol + Keys.ENTER)
                break
        if i >= 2:
            break
    # проверка того, что найдено более одного совпадения
    assert len(driver.find_elements_by_xpath('//div[@class="_3TF_T"]')) > 1
