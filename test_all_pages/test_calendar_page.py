import pytest
import time
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By


# текущий год
current_year = int((time.ctime(time.time()))[-4:])

# в фервале 29 дней, если код високосный, иначе 28
feb_days = 29 if current_year % 4 == 0 else 28

# словарь {"название месяца на сайте": ["название месяца в модуле time", "кол-во дней в месяце"]}
month_dict = {'янв.': ['Jan', 31], 'февр.': ['Feb', feb_days], 'март': ['Mar', 31], 'апр.': ['Apr', 30],
              'май': ['May', 31], 'июнь': ['Jun', 30], 'июль': ['Jul', 31], 'авг.': ['Aug', 31],
              'сент.': ['Sep', 30], 'окт.': ['Oct', 31], 'нояб.': ['Nov', 30], 'дек.': ['Dec', 31]}

# словарь {"название месяца": "его численное обозначение (для поисковика в календаре)"}
months_to_number = {'января': '01', 'февраля': '02', 'марта': '03', 'апреля': '04',
               'мая': '05', 'июня': '06', 'июля': '07', 'августа': '08',
               'сентября': '09', 'октября': '10', 'ноября': '11', 'декабря': '12'}


@pytest.fixture()
def driver(general_fixture):
    # переход на страницу календаря
    general_fixture.get('http://super-admin.tabolabs.com/calendar')
    WebDriverWait(general_fixture, 3).until(EC.visibility_of_element_located((By.XPATH, "//h1[contains(text(), 'Календарь публикаций')]")))
    yield general_fixture


def test_current_date(driver: WebDriver):
    """ ПРОВЕРКА ТЕКУЩЕЙ ДАТЫ """

    # поиск текущей даты, установленной в календаре (год, месяц, день)
    year = driver.find_element_by_xpath('//a[@title="Выбрать год"]').text
    month = driver.find_element_by_xpath('//a[@title="Выбрать месяц"]').text
    day = driver.find_element_by_xpath('//div[@aria-selected="true"]').text
    # сохраняем в переменную значение текущей даты, установленной на компьютере
    data = (time.ctime(time.time())).split(' ')
    # проверка идентичности текущей даты на компьютере и календаре сайта между собой
    assert day == data[3]
    assert month_dict[month][0] == data[1]
    assert year == data[-1]


def test_months_field(driver: WebDriver):
    """ ПРОВЕРКА СПИСКА МЕСЯЦЕВ В КАЛЕНДАРЕ """

    # сохраняем в список полные названия месяцев
    months = list(month_dict.keys())
    # открываем список месяцев в календаре сайта
    driver.find_element_by_xpath('//a[@title="Выбрать месяц"]').click()
    months_fields = driver.find_elements_by_xpath('//td[@role="gridcell"]/a')
    # проверка того, что список месяцев начинается с января
    assert months_fields[0].text == months[0]
    # проверка того, что кол-во месяцев в списке = 12
    assert len(months_fields) == 12
    # переходим на стартовую форму календаря, нажав на первый месяц (январь)
    driver.find_element_by_xpath('//td[@title="янв."]/a').click()
    # проверка правильной последовательности месяцев в списке (переключение месяцев от января то декабря)
    for i in range(12):
        checked_month = driver.find_element_by_xpath('//a[@title="Выбрать месяц"]').text
        assert checked_month == months[i]
        if i != 11:
            driver.find_element_by_xpath('//a[@title="Следующий месяц (PageDown)"]').click()


def test_days_field(driver: WebDriver):
    """ ПРОВЕРКА СПИСКА ДНЕЙ В КАЛЕНДАРЕ """

    # переход на список дней в январе
    driver.find_element_by_xpath('//a[@title="Выбрать месяц"]').click()
    driver.find_element_by_xpath('//td[@title="янв."]/a').click()
    full_months_names = list(months_to_number.keys())
    # проверка кол-ва дней в каждом месяце
    for i in range(12):
        # название выбранного месяца
        month_name = driver.find_element_by_xpath('//a[@title="Выбрать месяц"]').text
        # кол-во дней в выбранном месяце
        number_of_days = len(driver.find_elements_by_css_selector(f'td[title*="{full_months_names[i]}"]'))
        assert number_of_days == month_dict[month_name][1]
        if i != 11:
            # переключение на следующий месяц
            driver.find_element_by_xpath('//a[@title="Следующий месяц (PageDown)"]').click()


def test_year_field(driver: WebDriver):
    """ ПРОВЕРКА СПИСКА ГОДОВ """

    # установка текущего года на календаре сайта
    set_year = int(driver.find_element_by_xpath('//a[@title="Выбрать год"]').text)
    # переключение года
    for i in range(5):
        driver.find_element_by_xpath('//a[@title="Предыдущий год (Control + left)"]').click()
        last_year = int(driver.find_element_by_xpath('//a[@title="Выбрать год"]').text)
        # проверка того, что год меняется на 1
        assert set_year - last_year == 1
        set_year = last_year


def test_post_date_identity(driver: WebDriver):
    """ ПРОВЕРКА ИДЕНТИЧНОСТИ ДАТЫ ПОСТА НА СТРАНИЦЕ ПОСТОВ И НА СТРАНИЦЕ КАЛЕНДАРЯ """

    # переход на страницу постов
    driver.find_element_by_xpath('//li[@title="Посты"]/a').click()
    # определение даты создания последнего поста
    first_post_data = driver.find_elements_by_xpath('//div[@class="_1fewu"]')[0].text.split(' ')
    # переменные: год создания, месяц создания, день создания
    post_year, post_month, post_day = first_post_data[2], first_post_data[1], first_post_data[0]
    # определение имени последнего поста
    first_post_name = driver.find_elements_by_xpath('//div[@class="gSBJN"]/a')[0].text
    # определение url последнего поста
    driver.find_elements_by_xpath('//div[@class="gSBJN"]/a')[0].click()
    first_post_url = driver.current_url
    # переход на страницу календаря
    driver.find_element_by_xpath('//a[@href="/calendar"]').click()
    # ввод даты создания последнего поста в календаре
    driver.find_element_by_xpath('//input[@class="ant-calendar-input "]').send_keys(f'{post_year}-{months_to_number[post_month]}-{post_day}' + Keys.ENTER)
    # проверка наличия этого поста в появившемся списке
    checked_post_name = driver.find_elements_by_xpath('//div[@class="_3IYmw"]/div/div/div[1]//div[@class="gSBJN"]/a')[-1].text
    assert checked_post_name == first_post_name
    # переход на страницу этого поста
    driver.find_elements_by_xpath('//div[@class="_3IYmw"]/div/div/div[1]//div[@class="gSBJN"]/a')[-1].click()
    # проверка идентичности его url с url последнего поста на странице постов
    # (проверяем url ,т.к. названия разных постов могут быть одинаковы)
    assert driver.current_url == first_post_url
