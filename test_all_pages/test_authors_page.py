import pytest
import time
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException


# корректные имя и фамилия
name, surname = 'test_name', 'test_surname'
# некорректные имя и фамилия
invalid_name, invalid_surname = '1/. +$', '(#@&'


@pytest.fixture()
def driver(general_fixture: WebDriver):
    # переход на страницу авторов
    general_fixture.get('http://super-admin.tabolabs.com/authors')
    WebDriverWait(general_fixture, 3).until(EC.visibility_of_element_located((By.XPATH, "//h1[contains(text(), 'Авторы')]")))
    yield general_fixture


def test_create_author(driver: WebDriver):
    """СОЗДАНИЕ АВТОРА"""

    time.sleep(0.5)
    driver.find_element_by_class_name('_3ugjY').click()  # нажатие на создание автора
    # ввод корректных имени и фамилии
    driver.find_element_by_xpath('//input[@placeholder="Введите имя автора"]').send_keys(name)
    driver.find_element_by_xpath('//input[@placeholder="Введите фамилию автора"]').send_keys(surname)
    # нажатие на сохранение автора
    driver.find_element_by_xpath('//div[@class="AQzbu"]/button[1]').click()
    # проверка наличия созданного автора
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, f"//div[@class='_2DKS9']//span[contains(text(), '{name} {surname}')]")))


def test_edit_author(driver: WebDriver):
    """ РЕДАКТИРОВАНИЕ АВТОРА """

    """ Ввод пустых полей """
    # нажатие на редактирование автора
    driver.find_element_by_xpath('//div[@class="_2DKS9"]/div[1]//button[1]').click()
    # очищение полей имени и фамилии соответственно
    driver.find_element_by_xpath('//input[@placeholder="Введите имя автора"]').send_keys(Keys.CONTROL+'a'+Keys.BACKSPACE)
    driver.find_element_by_xpath('//input[@placeholder="Введите фамилию автора"]').send_keys(Keys.CONTROL+'a'+Keys.BACKSPACE)
    # нажатие на сохранение автора
    driver.find_element_by_xpath('//div[@class="AQzbu"]/button[1]').click()
    # проверка того что имя и фамилия не изменились, так как введены пустые данные
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, f"//div[@class='_2DKS9']//span[contains(text(), '{name} {surname}')]")))

    """ Ввод некорректных данных """
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, '//div[@class="_2DKS9"]/div[1]//button[1]')))
    time.sleep(0.5)
    # нажатие на редактирование автора
    driver.find_element_by_xpath('//div[@class="_2DKS9"]/div[1]//button[1]').click()
    # очищение полей имени и фамилии соответственно
    driver.find_element_by_xpath('//input[@placeholder="Введите имя автора"]').send_keys(Keys.CONTROL+'a'+Keys.BACKSPACE)
    driver.find_element_by_xpath('//input[@placeholder="Введите фамилию автора"]').send_keys(Keys.CONTROL+'a'+Keys.BACKSPACE)
    # ввод некорректных имени и фамилии
    driver.find_element_by_xpath('//input[@placeholder="Введите имя автора"]').send_keys(invalid_name)
    driver.find_element_by_xpath('//input[@placeholder="Введите фамилию автора"]').send_keys(invalid_surname)
    # нажатие на сохранение автора
    driver.find_element_by_xpath('//div[@class="AQzbu"]/button[1]').click()
    # проверка того что имя и фамилия автора поменялись на некорректные данные
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, f"//div[@class='_2DKS9']//span[contains(text(), '{invalid_name} {invalid_surname}')]")))


def test_delete_author(driver: WebDriver):
    """ УДАЛЕНИЕ АВТОРА """

    WebDriverWait(driver, 3).until(EC.element_to_be_clickable((By.XPATH, '//div[@class="_2DKS9"]/div[1]//button[1]')))
    # нажатие на редактирование автора
    driver.find_element_by_xpath('//div[@class="_2DKS9"]/div[1]//button[1]').click()
    WebDriverWait(driver, 3).until(EC.element_to_be_clickable((By.XPATH, '//div[@class="AQzbu"]/button[3]')))
    # нажатие на кнопку "Удалить автора"
    driver.find_element_by_xpath('//div[@class="AQzbu"]/button[3]').click()
    # проверка того что автор удален
    WebDriverWait(driver, 3).until_not(EC.visibility_of_element_located((By.XPATH, f"//div[@class='_2DKS9']//span[contains(text(), '{invalid_name} {invalid_surname}')]")))


def test_finding_authors(driver: WebDriver):
    """ ПОИСК АВТОРА """

    """ Сбор всех авторов """
    # скролл вниз меню разделов (чтобы следующие скроллы относились к самой странице)
    driver.find_element_by_tag_name('body').send_keys(Keys.END)
    for i in range(5):
        # сбор всех отображенных блоков авторов
        all_authors = driver.find_elements_by_xpath('//span[@class="_19CfM"]')
        # скролл страницы вниз
        driver.find_element_by_tag_name('body').send_keys(Keys.END)
        # проверка того что загрузились новые блоки авторов, если не загрузились то прерываем скроллинг
        try:
            WebDriverWait(driver, 2).until(EC.visibility_of_element_located((By.XPATH, '//div[@class="_2DKS9"]/div[{}]//span[@class="_19CfM"]'.format(len(all_authors)+1))))
        except TimeoutException:
            break

    # Сбор имен и фамилий всех отображенных авторов
    all_authors_names = [author.text for author in driver.find_elements_by_xpath('//span[@class="_19CfM"]')]
    # преобразование списка имен и фамилий авторов в строку
    all_authors_names_str = " ".join(all_authors_names)
    # получение всех символов (по одному экземпляру), использованных в именах и фамилиях авторов
    all_symbols = set(list(all_authors_names_str))

    """ Поиск авторов без совпадений """
    for i in range(5):
        # ввод текста и нажатие на ENTER
        driver.find_element_by_xpath('//input[@placeholder="Поиск"]').send_keys('TestAuthor' + Keys.ENTER)
        # проверка того что совпадений нет, в противном случае снова добавляем текст и так 5 раз
        try:
            WebDriverWait(driver, 3).until_not(EC.visibility_of_element_located((By.XPATH, '//div[@class="_3PGYQ"]')))
            break
        except TimeoutException:
            continue
    else:
        raise TimeoutException
    # очищение поля поиска
    driver.find_element_by_xpath('//input[@placeholder="Поиск"]').send_keys(Keys.CONTROL+'a'+Keys.BACKSPACE)

    """ Поиск авторов с одним совпадением """
    for nm in all_authors_names:
        # Если имя автора встречается в списке всех имен и фамилий единожды, то вводим это имя и нажимаем ENTER
        if all_authors_names_str.count(nm[:nm.index(' ')]) == 1:
            driver.find_element_by_xpath('//input[@placeholder="Поиск"]').send_keys(nm[:nm.index(' ')] + Keys.ENTER)
            break
        # Если фамилия автора встречается в списке всех имен и фамилий единожды, то вводим эту фамилию и нажимаем ENTER
        if all_authors_names_str.count(nm[nm.index(' ')+1:]) == 1:
            driver.find_element_by_xpath('//input[@placeholder="Поиск"]').send_keys(nm[nm.index(' ')+1:] + Keys.ENTER)
            break
    # проверка того что найдено лишь одно совпадение
    assert len(driver.find_elements_by_xpath('//div[@class="_3PGYQ"]')) == 1
    # очищение поля поиска
    driver.find_element_by_xpath('//input[@placeholder="Поиск"]').send_keys(Keys.CONTROL+'a'+Keys.BACKSPACE)

    """ Поиск авторов с несколькими совпадениями """
    # Если символ встречается в списке названий авторов более одного раза, то вводим его в поле поиска
    for symbol in all_symbols:
        i = 0
        for nm in all_authors_names:
            if symbol in nm:
                i += 1
            if i >= 2:
                # ввод символа и нажатие на ENTER
                driver.find_element_by_xpath('//input[@placeholder="Поиск"]').send_keys(symbol + Keys.ENTER)
                break
        if i >= 2:
            break
    # проверка того что найдено более одного совпадения
    assert len(driver.find_elements_by_xpath('//div[@class="_3PGYQ"]')) > 1
