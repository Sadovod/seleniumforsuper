import pytest
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException


@pytest.fixture()
def driver(general_fixture: WebDriver):
    # переход на страницу постов
    general_fixture.get('http://super-admin.tabolabs.com/posts')
    WebDriverWait(general_fixture, 3).until(EC.visibility_of_element_located((By.XPATH, "//h1[contains(text(), 'Посты')]")))
    yield general_fixture


def test_posts_create(driver: WebDriver):
    """ ПЕРЕХОД НА СОЗДАНИЕ ПОСТА """

    # нажатие на кнопку создания поста
    driver.find_element_by_xpath('//div[@class="_26WcI"]/div/button').click()
    # ожидание появления страницы создания поста
    WebDriverWait(driver, 3).until(EC.visibility_of_any_elements_located((By.XPATH, '//h1[contains(text(), "Добавление нового поста")]')))


def test_post_edit_through_edit_button(driver: WebDriver):
    """ ПЕРЕХОД НА РЕДАКТИРОВАНИЕ ПОСТА ЧЕРЕЗ КНОПКУ РЕДАКТИРОВАНИЯ """

    # ожидание наличия постов
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, "//div[@class='_1b-uQ']/div[1]")))
    # нажатие на кнопку "редактировать" первого поста
    driver.find_elements_by_xpath('//div[@class="_1b8Lp"]/button')[0].click()
    # ожидание появления страницы редактирования поста
    WebDriverWait(driver, 3).until((EC.visibility_of_element_located((By.XPATH, '//div[@class="_2LjT6"]//h1[contains(text(), "Редактирование поста")]'))))


def test_edit_post_through_title(driver: WebDriver):
    """ ПЕРЕХОД НА РЕДАКТИРОВАНИЕ ПОСТА ЧЕРЕЗ ЗАГОЛОВОК """

    # ожидание наличия постов
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, "//div[@class='_1b-uQ']/div[1]")))
    # нажатие на заголовок первого поста
    driver.find_elements_by_xpath('//div[@class="_1b-uQ"]//a')[0].click()
    # ожидание появления страницы редактирования поста
    WebDriverWait(driver, 3).until((EC.visibility_of_element_located((By.XPATH, '//div[@class="_2LjT6"]//h1[contains(text(), "Редактирование поста")]'))))


def test_view_author(driver: WebDriver):
    """ ПЕРЕХОД НА ПРЕДПРОСМОТР ПОСТА """

    # ожидание наличия постов
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, "//div[@class='_1b-uQ']/div[1]")))
    # нажатие на кнопку предпросмотра первого поста
    driver.find_elements_by_xpath('//div[@class="_2IDqA"]/button')[0].click()
    # проверка открытия новой вкладки
    assert len(driver.window_handles) == 2
    # переход на новую вкладку
    driver.switch_to.window(driver.window_handles[1])
    # проверка того, что новой вкладкой является страница постов сайта "super"
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, '//h3[contains(text(), "Главные темы")]')))
    # закрытие данной вкладки
    driver.close()
    # переход на первую вкладку
    driver.switch_to.window(driver.window_handles[0])


def test_filters(driver: WebDriver):
    """ ПРОВЕРКА ФИЛЬТРОВ В ПОИСКЕ ПОСТОВ """

    # нажатие на фильтр "Тип поста"
    driver.find_element_by_xpath('//div[@class="_2nYHy"]/div[1]//div[@role="combobox"]').click()
    WebDriverWait(driver, 3).until(EC.visibility_of(driver.find_elements_by_xpath('//ul[@role="listbox"]')[0]))
    # выбор первого появившегося типа поста
    driver.find_element_by_xpath('//div[@class="_2nYHy"]/div[1]//div[@role="combobox"]').send_keys(Keys.ENTER)

    # нажатие на фильтр "Рубрика"
    driver.find_element_by_xpath('//div[@class="_2nYHy"]/div[2]//div[@role="combobox"]').click()
    WebDriverWait(driver, 3).until(EC.visibility_of(driver.find_elements_by_xpath('//ul[@role="listbox"]')[1]))
    # выбор первой появившейся рубрики
    driver.find_element_by_xpath('//div[@class="_2nYHy"]/div[2]//div[@role="combobox"]').send_keys(Keys.ENTER)

    # нажатие на фильтр "Дата публикации"
    driver.find_elements_by_xpath('//input[@placeholder="Выберите дату"]')[0].click()
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, '//div[@class="ant-calendar"]')))

    # нажатие на фильтр "Дата изменения"
    driver.find_elements_by_xpath('//input[@placeholder="Выберите дату"]')[1].click()
    WebDriverWait(driver, 3).until(EC.visibility_of_element_located((By.XPATH, '//div[@class="ant-calendar"]')))

    # нажатие на фильтр "Источник информации"
    driver.find_element_by_xpath('//div[@class="_1NqFQ"]/input').click()
    # вводим слово "Wiki"
    driver.find_element_by_xpath('//div[@class="_1NqFQ"]/input').send_keys('Wiki')

    # нажатие на фильтр "Автор поста"
    driver.find_element_by_xpath('//div[@class="_2nYHy"]/div[6]//div[@role="combobox"]').click()
    WebDriverWait(driver, 3).until(EC.visibility_of(driver.find_elements_by_xpath('//ul[@role="listbox"]')[2]))

    # поочередное нажатие на фильтры-кнопки
    for i in range(1, 6):
        driver.find_elements_by_xpath('//input[@class="ant-checkbox-input"]')[i-1].click()
        WebDriverWait(driver, 2).until(EC.visibility_of_element_located((By.XPATH, f'//div[@class="_1NqFQ"]//label[{i}]/span[@class="ant-checkbox ant-checkbox-checked"]')))

    # нажатие на кнопку "принять" для фильтров
    driver.find_element_by_xpath('//div[@class="_1Omto"]/button[2]').click()
    # нажатие на кнопку "отменить" для фильтров
    driver.find_element_by_xpath('//div[@class="_1Omto"]/button[1]').click()


def test_finding_posts(driver: WebDriver):
    """ ПОИСК ПОСТОВ """

    # скролл вниз ( скролл меню разделов сайта вниз, чтобы в следующий раз скролл относился к странице постов)
    driver.find_element_by_tag_name('body').send_keys(Keys.END)
    # скролл вниз до 5 раз для определения всех постов
    for i in range(5):
        # добавление в список всех отображенных постов
        all_posts = driver.find_elements_by_xpath('//div[@class="_1b-uQ"]//a')
        # скролл вниз
        driver.find_element_by_tag_name('body').send_keys(Keys.END)
        # проверка того, что загрузились новые посты, в противном случае - завершение цикла
        try:
            WebDriverWait(driver, 3).until(EC.visibility_of_element_located(
                (By.XPATH, '//div[@class="_1b-uQ"]/div[{}]//a'.format(len(all_posts)+1))))
        except TimeoutException:
            break

    # сохранение в список названий всех отображенных постов
    all_posts_names = [post.text for post in driver.find_elements_by_xpath('//div[@class="_1b-uQ"]//a')]
    # представление списка названий постов в виде строки
    all_posts_names_str = " ".join(all_posts_names)
    # сохранение в множество всех символов (в единичном экземпляре), использованных в названиях отображенных постов
    all_symbols = set(list(all_posts_names_str))

    """ Поиск постов без совпадений """
    # ввод текста в поле поиска и нажатие на ENTER, если имеются совпадения - повторение операции
    # и так до 7 раз
    for i in range(7):
        driver.find_element_by_xpath('//input[@placeholder="Поиск"]').send_keys('TestPost' + Keys.ENTER)
        # проверка наличия совпадений
        try:
            WebDriverWait(driver, 3).until_not(EC.visibility_of_all_elements_located((By.CLASS_NAME, '_2jMXB')))
            break
        except TimeoutException:
            continue
    else:
        raise TimeoutException
    # очищение поля поиска постов
    driver.find_element_by_xpath('//input[@placeholder="Поиск"]').send_keys(Keys.CONTROL+'a'+Keys.BACKSPACE)

    """ Поиск постов с одним совпадением """
    # если название поста встречается единожды в списке названий всех постов,
    # то вводим это название в поле поиска постов и нажимаем ENTER
    for nm in all_posts_names:
        if all_posts_names.count(nm) == 1:
            # ввод названия поста и нажатие на ENTER
            driver.find_element_by_xpath('//input[@placeholder="Поиск"]').send_keys(nm + Keys.ENTER)
            break
    # проверка того, что найден только один пост
    assert len(driver.find_elements_by_xpath("//div[@class='_1vY44']")) == 1
    # очищение поля поиска постов
    driver.find_element_by_xpath('//input[@placeholder="Поиск"]').send_keys(Keys.CONTROL + 'a' + Keys.BACKSPACE)

    """ Поиск постов с несколькими совпадениями """
    # поиск символа, который встречается сразу в нескольких названиях постов и ввод этого символа
    # с последующим нажатием на ENTER
    for symbol in all_symbols:
        i = 0
        for n in all_posts_names:
            if symbol in n:
                i += 1
            if i >= 2:
                # ввод символа и нажатие на ENTER
                driver.find_element_by_xpath('//input[@placeholder="Поиск"]').send_keys(symbol + Keys.ENTER)
                break
        if i >= 2:
            break
    # проверка того, что найдено более одного поста
    assert len(driver.find_elements_by_xpath("//div[@class='_1vY44']")) > 1
