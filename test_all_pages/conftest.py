import pytest
from selenium.webdriver.chrome.webdriver import WebDriver
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from configurations import PATH_TO_DRIVER


@pytest.fixture(scope="module")
def general_fixture():
    # запуск chrome драйвера
    chrome_driver = WebDriver(executable_path=f'{PATH_TO_DRIVER}')
    chrome_driver.implicitly_wait(7)
    # переход на страницу авторизации
    chrome_driver.get('http://super-admin.tabolabs.com/')
    # ввод верного логина
    chrome_driver.find_element_by_id('normal_login_email').send_keys('editor@super.ru')
    # ввод верного пароля
    chrome_driver.find_element_by_id('normal_login_password').send_keys('123321')
    # нажатие на кнопку "Войти"
    chrome_driver.find_element_by_xpath('//button[@type="submit"]').click()
    WebDriverWait(chrome_driver, 5).until(EC.visibility_of_element_located((By.XPATH, '//div[@class="_3zlYu"]//div[text()="Профессиональный Редактор"]')))
    yield chrome_driver
    chrome_driver.quit()
