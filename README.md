# Test for "Super"

  
### INSTALLATION

1. Press on **Download zip**
2. Download [Chrome driver][1]
3. Install [Python 3.7.4][2]
4. Install *REQUIREMENTS*
-------------------------

### REQUIREMENTS

1. *selenium==3.141.0*
2. *pytest==5.1.2*
--------------------

### TEST RUN

Open file ***configurations.py*** and replace variable values with full paths to the following files:

1. PATH_TO_DRIVER: *specify the path to Chrome Driver*
2. PATH_TO_PICTURE: ***Test_Picture.jpg***
3. PATH_TO_PICTURE2: ***Test_Picture2.jpg***
4. PATH_TO_VIDEO: ***Test_Video.mp4***
5. PATH_TO_WIDGET_SCREENSHOT: *specify the folder where you want to save the screenshot of the widget*

Then open **cmd.exe** in directory with this project and print: ***run_tests.py***

**Note:** don't use the mouse while running tests

[1]: https://chromedriver.chromium.org/downloads
[2]: https://www.python.org/downloads/release/python-374/

