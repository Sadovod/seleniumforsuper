import pytest


if __name__ == '__main__':
	# запуск тестов
    pytest.main(["-v", "--tb=short", "--resultlog=tests_traceback.txt", "test_all_pages", "test_login_page"])
